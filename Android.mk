LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

SVNVERSION := $(shell $(LOCAL_PATH)/util/getrevision.sh -u)

RELEASE := 0.9.7
VERSION := $(RELEASE)-$(SVNVERSION)
RELEASENAME ?= $(VERSION)

LOCAL_MODULE := flashrom

LOCAL_MODULE_TARGET_ARCH := arm64
LOCAL_MODULE_PATH := $(TARGET_RECOVERY_ROOT_OUT)/sbin


#LOCAL_MODULE_TAGS := optional
# FIXME Should add arm64 support in hwaccess.h instead of doing that
LOCAL_CFLAGS += -D'FLASHROM_VERSION="$(VERSION)"'
LOCAL_CFLAGS += -D'CONFIG_DEFAULT_PROGRAMMER=PROGRAMMER_INVALID'
LOCAL_CFLAGS += -D'CONFIG_DEFAULT_PROGRAMMER_ARGS=""'
# We only need linux_spi programmer
LOCAL_CFLAGS += -D'CONFIG_LINUX_SPI=1'

LOCAL_CFLAGS += -D'__ARMEL__'
LOCAL_CFLAGS += -Wno-unused-parameter -Wno-pointer-arith -Wno-sign-compare

# Library code
LOCAL_SRC_FILES := libflashrom.c layout.c flashrom.c udelay.c programmer.c helpers.c ich_descriptors.c fmap.c

# Flash chip drivers
LOCAL_SRC_FILES += jedec.c stm50.c w39.c w29ee011.c \
	sst28sf040.c 82802ab.c \
	sst49lfxxxc.c sst_fwhub.c edi.c flashchips.c spi.c spi25.c spi25_statusreg.c \
	opaque.c sfdp.c en29lv640b.c at45db.c

# Programmers
LOCAL_SRC_FILES += cli_classic.c cli_output.c cli_common.c print.c

LOCAL_SRC_FILES += linux_spi.c

LOCAL_STATIC_LIBRARIES += libstdc++ libc libm

LOCAL_FORCE_STATIC_EXECUTABLE := true

include $(BUILD_EXECUTABLE)
